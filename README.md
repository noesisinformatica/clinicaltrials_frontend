# Clinical Trials Frontend Application

This is a [AngularJs](http://angularjs.org) application that serves as a frontend for registry of clinical trials. It uses [Termlex](https://termlex.gitlab.io) 
as the terminology server to use SNOMED CT codes as conditions and treatments included in the trial protocols.

This project depends on the backend component at [https://gitlab.com/noesisinformatica/clinicaltrials_example](https://gitlab.com/noesisinformatica/clinicaltrials_example)

#### Authors
  * Jay Kola

#### License
  * [Noesis Informatica License](https://noesisinformatica.com/licenses/LICENSE-1.0.txt)
  
## Running Locally
Download the `demo.zip` file by [clicking here](https://gitlab.com/noesisinformatica/clinicaltrials_frontend/raw/master/demo.zip) or clicking on it.

Then extract the zip file and navigate to the extracted folder.

```
npm install http-server -g
http-server
```

Now open [http://localhost:8080](http://localhost:8080) in your favourite browser to view the demo. We use http-server 
to address a silly 'base' reference used in this project -- inherited via the generator we used

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should go to your checked out repo and run the following command

    npm install

## Running the example

 - Run the [backend project](https://gitlab.com/noesisinformatica/clinicaltrials_example) in a separate terminal window first with command `sails lift`

 - Run the project with `gulp serve-dev --sync`

 - `--sync` opens it in a browser and updates the browser with any files changes.

### Linting
 - Run code analysis using `gulp vet`. This runs jshint, jscs, and plato.

### Tests
 - Run the unit tests using `gulp test` (via karma, mocha, sinon).

### Building the project
 - Build the optimized project using `gulp build`
 - This create the optimized code for the project and puts it in the build folder

### Running the optimized code
 - Run the optimize project from the build folder with `gulp serve-build`

## Structure
The structure also contains a gulpfile.js and a server folder. The server is there just so we can serve the app using node. Feel free to use any server you wish.

	/src
		/client
			/app
			/content
	
## Release updates

If you want to make changes and create updated version, then use [gulp-release](https://www.npmjs.com/package/gulp-release)

```
// perform release -- increment version and commit changes using gitflow model
gulp release
```

Happy Hacking!
