(function() {
    'use strict';

    angular
        .module('app.clinicalcentre')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'listClinicalcentre',
                config: {
                    url: '/clinicalcentre',
                    templateUrl: 'app/clinicalcentre/views/list.html',
                    controller: 'ClinicalcentreController',
                    controllerAs: 'vm',
                    title: 'List Clinicalcentres',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-folder-open"></i> Clinicalcentres'
                    }
                }
            },
            {
                state: 'createClinicalcentre',
                config: {
                    url: '/clinicalcentre/create',
                    templateUrl: 'app/clinicalcentre/views/create.html',
                    controller: 'ClinicalcentreController',
                    controllerAs: 'vm',
                    title: 'Create Clinicalcentre'
                }
            },
            {
                state: 'viewClinicalcentre',
                config: {
                    url: '/clinicalcentre/:clinicalcentreId',
                    templateUrl: 'app/clinicalcentre/views/view.html',
                    controller: 'ClinicalcentreController',
                    controllerAs: 'vm',
                    title: 'View Clinicalcentre'
                }
            },
            {
                state: 'editClinicalcentre',
                config: {
                    url: '/clinicalcentre/:clinicalcentreId/edit',
                    templateUrl: 'app/clinicalcentre/views/edit.html',
                    controller: 'ClinicalcentreController',
                    controllerAs: 'vm',
                    title: 'Edit Clinicalcentre'
                }
            }
        ];
    }
})();
