(function() {
    'use strict';

    angular
        .module('app.clinicalcentre')
        .factory('Clinicalcentre', Clinicalcentre);

    Clinicalcentre.$inject = ['$resource', 'API_BASE_URL'];
    /* @ngInject */
    function Clinicalcentre($resource, API_BASE_URL) {

        var params = {
            clinicalcentreId: '@id'
        };

        var actions = {
            update: {
                method: 'PUT'
            }
        };

        var API_URL = API_BASE_URL + '/clinicalcentre/:clinicalcentreId';

        return $resource(API_URL, params, actions);

    }

})();
