(function() {
    'use strict';

    angular
        .module('app.clinicalcentre')
        .factory('ClinicalcentreForm', factory);

    function factory() {

        var getFormFields = function(disabled) {

            var fields = [
                {
                    key: 'name',
                    type: 'input',
                    templateOptions: {
                        label: 'Name:',
                        disabled: disabled,
                        required: true
                    }
                },
                {
                    key: 'addressLine1',
                    type: 'input',
                    templateOptions: {
                        label: 'Address Line 1:',
                        disabled: disabled
                    }
                },
                {
                    key: 'addressLine2',
                    type: 'input',
                    templateOptions: {
                        label: 'Address Line 2:',
                        disabled: disabled
                    }
                },
                {
                    key: 'postCode',
                    type: 'input',
                    templateOptions: {
                        label: 'Post Code:',
                        disabled: disabled
                    }
                },
                {
                    key: 'openingTimes',
                    type: 'input',
                    templateOptions: {
                        label: 'Opening Times:',
                        disabled: disabled
                    }
                }
            ];

            return fields;

        };

        var service = {
            getFormFields: getFormFields
        };

        return service;

    }

})();
