(function () {
    'use strict';

    angular.module('app.clinicalcentre', []);
    angular.module('app').requires.push('app.clinicalcentre');

})();
