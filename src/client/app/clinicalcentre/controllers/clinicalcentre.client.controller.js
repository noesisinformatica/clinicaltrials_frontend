(function () {
    'use strict';

    angular
        .module('app.clinicalcentre')
        .controller('ClinicalcentreController', ClinicalcentreController);

    ClinicalcentreController.$inject = ['logger',
        '$stateParams',
        '$location',
        'Clinicalcentre',
        'TableSettings',
        'ClinicalcentreForm'];
    /* @ngInject */
    function ClinicalcentreController(logger,
        $stateParams,
        $location,
        Clinicalcentre,
        TableSettings,
        ClinicalcentreForm) {

        var vm = this;

        vm.tableParams = TableSettings.getParams(Clinicalcentre);
        vm.clinicalcentre = {};

        vm.setFormFields = function(disabled) {
            vm.formFields = ClinicalcentreForm.getFormFields(disabled);
        };

        vm.create = function() {
            // Create new Clinicalcentre object
            var clinicalcentre = new Clinicalcentre(vm.clinicalcentre);

            // Redirect after save
            clinicalcentre.$save(function(response) {
                logger.success('Clinicalcentre created');
                $location.path('clinicalcentre/' + response.id);
            }, function(errorResponse) {
                vm.error = errorResponse.data.summary;
            });
        };

        // Remove existing Clinicalcentre
        vm.remove = function(clinicalcentre) {

            if (clinicalcentre) {
                clinicalcentre = Clinicalcentre.get({
                    clinicalcentreId:clinicalcentre.id}, function() {
                    clinicalcentre.$remove(function() {
                        logger.success('Clinicalcentre deleted');
                        vm.tableParams.reload();
                    });
                });
            } else {
                vm.clinicalcentre.$remove(function() {
                    logger.success('Clinicalcentre deleted');
                    $location.path('/clinicalcentre');
                });
            }

        };

        // Update existing Clinicalcentre
        vm.update = function() {
            var clinicalcentre = vm.clinicalcentre;

            clinicalcentre.$update(function() {
                logger.success('Clinicalcentre updated');
                $location.path('clinicalcentre/' + clinicalcentre.id);
            }, function(errorResponse) {
                vm.error = errorResponse.data.summary;
            });
        };

        vm.toViewClinicalcentre = function() {
            vm.clinicalcentre = Clinicalcentre.get({
                clinicalcentreId: $stateParams.clinicalcentreId});
            vm.setFormFields(true);
        };

        vm.toEditClinicalcentre = function() {
            vm.clinicalcentre = Clinicalcentre.get({
                clinicalcentreId: $stateParams.clinicalcentreId});
            vm.setFormFields(false);
        };

        activate();

        function activate() {
            //logger.info('Activated Clinicalcentre View');
        }
    }

})();
