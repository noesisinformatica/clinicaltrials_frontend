(function () {
    'use strict';

    angular.module('app.condition', []);
    angular.module('app').requires.push('app.condition');

})();
