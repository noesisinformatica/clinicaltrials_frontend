(function() {
    'use strict';

    angular
        .module('app.condition')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'listCondition',
                config: {
                    url: '/condition',
                    templateUrl: 'app/condition/views/list.html',
                    controller: 'ConditionController',
                    controllerAs: 'vm',
                    title: 'List Conditions',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-folder-open"></i> Conditions'
                    }
                }
            },
            {
                state: 'createCondition',
                config: {
                    url: '/condition/create',
                    templateUrl: 'app/condition/views/create.html',
                    controller: 'ConditionController',
                    controllerAs: 'vm',
                    title: 'Create Condition'
                }
            },
            {
                state: 'viewCondition',
                config: {
                    url: '/condition/:conditionId',
                    templateUrl: 'app/condition/views/view.html',
                    controller: 'ConditionController',
                    controllerAs: 'vm',
                    title: 'View Condition'
                }
            },
            {
                state: 'editCondition',
                config: {
                    url: '/condition/:conditionId/edit',
                    templateUrl: 'app/condition/views/edit.html',
                    controller: 'ConditionController',
                    controllerAs: 'vm',
                    title: 'Edit Condition'
                }
            }
        ];
    }
})();
