(function() {
    'use strict';

    angular
        .module('app.condition')
        .factory('Condition', Condition);

    Condition.$inject = ['$resource', 'API_BASE_URL'];
    /* @ngInject */
    function Condition($resource, API_BASE_URL) {

        var params = {
            conditionId: '@id'
        };

        var actions = {
            update: {
                method: 'PUT'
            },
            // we need to over ride post/save url since we are reusing snomed code as condition id
            // default behaviour is for id to be autogeneratd by backend
            save: {method:'POST', url: API_BASE_URL + '/condition'}
        };

        var API_URL = API_BASE_URL + '/condition/:conditionId';

        return $resource(API_URL, params, actions);

    }

})();
