(function() {
    'use strict';

    angular
        .module('app.condition')
        .factory('ConditionForm', factory);

    function factory() {

        var getFormFields = function(disabled) {

            var fields = [
                {
                    key: 'name',
                    type: 'input',
                    templateOptions: {
                        label: 'Name:',
                        disabled: disabled
                    }
                },
                {
                    key: 'state',
                    type: 'input',
                    templateOptions: {
                        label: 'State:',
                        disabled: disabled
                    }
                },
                {
                    key: 'conditionAge',
                    type: 'input',
                    templateOptions: {
                        label: 'Condition Age:',
                        disabled: disabled
                    }
                }
            ];

            return fields;

        };

        var service = {
            getFormFields: getFormFields
        };

        return service;

    }

})();
