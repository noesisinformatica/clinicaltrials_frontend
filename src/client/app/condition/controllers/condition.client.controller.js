(function () {
    'use strict';

    angular
        .module('app.condition')
        .controller('ConditionController', ConditionController);

    ConditionController.$inject = ['logger', '$scope',
        '$stateParams',
        '$location',
        'Condition',
        'TableSettings',
        'ConditionForm',
        'termlexSearch'];
    /* @ngInject */
    function ConditionController(logger, $scope,
        $stateParams,
        $location,
        Condition,
        TableSettings,
        ConditionForm,
        termlexSearch) {

        var vm = this;

        vm.tableParams = TableSettings.getParams(Condition);
        vm.condition = {};

        // some basic parameter supported by search end point - refer to http://termlex.org/docs/ for more
        $scope.resultOpts = {
            maxInstantResultsSize: 10,
            conceptType: '919191919191441993',
            maxResultsSize: 100
        };

        // function that sets condition attributes when user selects item from results dropdown
        vm.onLiveSelect = function (item, model, label) {
            vm.condition.name = label;
            vm.condition.id = parseInt(item.id);
        };

        // function that handles live results - based on bootstrap type ahead
        vm.getMatches = function (searchTerm) {
            return termlexSearch.getFindings(searchTerm, $scope.resultOpts);
        };

        vm.setFormFields = function(disabled) {
            vm.formFields = ConditionForm.getFormFields(disabled);
        };

        vm.create = function() {
            // Create new Condition object
            var condition = new Condition(vm.condition);

            // Redirect after save
            condition.$save(function(response) {
                logger.success('Condition created');
                $location.path('condition/' + response.id);
            }, function(errorResponse) {
                vm.error = errorResponse.data.summary;
            });
        };

        // Remove existing Condition
        vm.remove = function(condition) {

            if (condition) {
                condition = Condition.get({conditionId:condition.id}, function() {
                    condition.$remove(function() {
                        logger.success('Condition deleted');
                        vm.tableParams.reload();
                    });
                });
            } else {
                vm.condition.$remove(function() {
                    logger.success('Condition deleted');
                    $location.path('/condition');
                });
            }

        };

        // Update existing Condition
        vm.update = function() {
            var condition = vm.condition;

            condition.$update(function() {
                logger.success('Condition updated');
                $location.path('condition/' + condition.id);
            }, function(errorResponse) {
                vm.error = errorResponse.data.summary;
            });
        };

        vm.toViewCondition = function() {
            vm.condition = Condition.get({conditionId: $stateParams.conditionId});
            vm.setFormFields(true);
        };

        vm.toEditCondition = function() {
            vm.condition = Condition.get({conditionId: $stateParams.conditionId});
            vm.setFormFields(false);
        };

        activate();

        function activate() {
            //logger.info('Activated Condition View');
        }
    }

})();
