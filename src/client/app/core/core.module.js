(function () {
    'use strict';

    angular
        .module('app.core', [
            'ngAnimate',
            'ngSanitize',
            'blocks.exception',
            'blocks.logger',
            'blocks.router',
            'ui.router',
            'ngplus',
            'ngResource',
            'ui.bootstrap',
            'ngTable',
            'formly',
            'formlyBootstrap',
            'ui.bootstrap', // we add ui.bootstrap to easy create ui components like dropdown search
            'termlex'       // termlex module provided by termlex-angularjs-module
        ]);
})();
