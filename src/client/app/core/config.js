(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[clinicaltrials_frontend Error] ',
        appTitle: 'clinicaltrials_frontend'
    };

    core.value('config', config);

    core.config(configure);

    // we have to inject termlexConfigProvider so we can set the url of the termlex instance we are connecting to
    configure.$inject = [
        '$logProvider',
        'routerHelperProvider',
        'exceptionHandlerProvider',
        'termlexConfigProvider'];
    /* @ngInject */
    function configure(
            $logProvider,
            routerHelperProvider,
            exceptionHandlerProvider,
            termlexConfigProvider) {

        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({docTitle: config.appTitle + ': '});
        termlexConfigProvider.url = 'http://termlex.org';   // set url for termlex instance
    }

})();
